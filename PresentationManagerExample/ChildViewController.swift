//
//  ChildViewController.swift
//  PresentationManager
//
//  Created by sai on 06/04/2017.
//

import UIKit

class ChildViewController: UIViewController {
    weak var delegate: ChildViewControllerDelegate?

    @IBAction func onButtonTapped() {
        self.delegate?.childViewControllerDidButtonTapped(self)
    }
}

protocol ChildViewControllerDelegate: class {
    func childViewControllerDidButtonTapped(_ viewController: ChildViewController)
}
