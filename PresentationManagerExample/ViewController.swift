//
//  ViewController.swift
//  PresentationManagerExample
//
//  Created by sai on 05/04/2017.
//

import UIKit
import PresentationManager

class ViewController: UITableViewController {
    
    @IBOutlet weak var directionSegmentedControl: UISegmentedControl!
    @IBOutlet weak var widthSwitch: UISwitch!
    @IBOutlet weak var widthSlider: UISlider!
    @IBOutlet weak var widthLabel: UILabel!
    @IBOutlet weak var heightSwitch: UISwitch!
    @IBOutlet weak var heightSlider: UISlider!
    @IBOutlet weak var heightLabel: UILabel!
    
    private var presentationManager: PresentationManager!
    
    private var direction: PresentationDirection {
        switch self.directionSegmentedControl.selectedSegmentIndex {
        case 0:
            return .left
        case 1:
            return .right
        case 2:
            return .top
        case 3:
            return .bottom
        case 4:
            return .center
        default:
            return .center
        }
    }
    private var widthRatio: CGFloat? {
        guard self.widthSlider.isEnabled else {
            return nil
        }
        return CGFloat(self.widthSlider.value)
    }
    private var heightRatio: CGFloat? {
        guard self.heightSlider.isEnabled else {
            return nil
        }
        return CGFloat(self.heightSlider.value)
    }
    
    private func updatePresentationManager() {
        let presentationManager = PresentationManager(direction: self.direction, presentationSize: PresentationSize(widthRatio: self.widthRatio, heightRatio: self.heightRatio))
        presentationManager.delegate = self
        self.presentationManager = presentationManager
    }
    
    @IBAction func onShowButtonTapped() {
        self.updatePresentationManager()
        
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "Child") as! ChildViewController
        viewController.delegate = self
        viewController.modalPresentationStyle = .custom
        viewController.transitioningDelegate = self.presentationManager
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func onNavButtonTapped() {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "Child") as! ChildViewController
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func onSwitchChanged(_ sender: UISwitch) {
        switch sender {
        case self.widthSwitch:
            self.widthSlider.isEnabled = sender.isOn
        case self.heightSwitch:
            self.heightSlider.isEnabled = sender.isOn
        default: ()
        }
    }
    
    @IBAction func onSliderChanged(_ sender: UISlider) {
        switch sender {
        case self.widthSlider:
            self.widthLabel.text = String(format: "%.0f%%", sender.value * 100)
        case self.heightSlider:
            self.heightLabel.text = String(format: "%.0f%%", sender.value * 100)
        default: ()
        }
    }
}

extension ViewController: ChildViewControllerDelegate {
    func childViewControllerDidButtonTapped(_ viewController: ChildViewController) {
        if let navigationController = viewController.navigationController {
            navigationController.popViewController(animated: true)
        } else if (viewController.presentingViewController != nil) {
            viewController.dismiss(animated: true, completion: nil)
        }
    }
}

extension ViewController: PresentationManagerDelegate {
    func presentationManager(_ manager: PresentationManager, presentationController: PresentationController, didTapDimmingView dimmingView: UIView) {
        presentationController.presentingViewController.dismiss(animated: true)
    }
}
