import XCTest

import ls_presentationmanagerTests

var tests = [XCTestCaseEntry]()
tests += ls_presentationmanagerTests.allTests()
XCTMain(tests)
