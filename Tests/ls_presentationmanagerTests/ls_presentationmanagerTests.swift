import XCTest
@testable import ls_presentationmanager

final class ls_presentationmanagerTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(ls_presentationmanager().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
