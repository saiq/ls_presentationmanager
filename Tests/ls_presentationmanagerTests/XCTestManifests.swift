import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(ls_presentationmanagerTests.allTests),
    ]
}
#endif
